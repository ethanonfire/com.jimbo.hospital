// BackAidlListener.aidl
package com.jimbo.project;

// Declare any non-default types here with import statements

interface BackAidlListener {
  void onBack(String packageName,int status);
}
