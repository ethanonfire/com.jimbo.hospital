package com.jimbo.hospital;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

import com.jimbo.jimbohttp.HttpBaseUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements MyApplication.AidlServiceListener {

    ImageButton button1;
    ImageButton button2;
    ImageButton button3;
    ImageButton backButton;
    public static String token;
    public static String objid;
    public static String sn;
    public static String key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MyApplication.getApplication().setAidlServiceListener(this);
        //token: 9a1bff5e-9775-4e82-82ba-0fde0fac04b2 objid: dc922091-bf07-4016-99ac-a7cd2e9a16e3 sn: F3B1C10001 key: 80d0a5a6-d3d3-4c23-a66c-03da9e5bd12a
//        HttpBaseUtil.getHttpBaseUtil().setAllData("c9b073af-2a12-4067-97c7-385ee00cae89",
//                "dc922091-bf07-4016-99ac-a7cd2e9a16e3", "F3B1C10001", "80d0a5a6-d3d3-4c23-a66c-03da9e5bd12a");

        button1 = findViewById(R.id.AAA);
        button2 = findViewById(R.id.BBB);
        button3 = findViewById(R.id.CCC);
        backButton = findViewById(R.id.iv_back_app);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), HospitalIntroActivity.class);
                //    intent.putExtra("state", 1);
                startActivity(intent);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DepartmentActivity.class);
                //  intent.putExtra("state", 2);
                startActivity(intent);
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DoctorsActivity.class);
                //intent.putExtra("state", 3);
                startActivity(intent);
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onBindSuccess() {
        Map<String, String> map = new HashMap<>();
        map.put("robotId", AIDLUtil.objid);
        map.put("isChinese", "1");
        HttpBaseUtil.getHttpBaseUtil().post2("department/getDepartmentList", map,
                this, new HttpBaseUtil.HttpListener() {
                    @Override
                    public void onSecussful(String msg) {
                        try {
                            JSONObject response = new JSONObject(msg);
                            Log.d("MainActivity response1", response.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFail(int i, Exception e, String s) {
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(s);
                            Log.d("MainActivity fail1", jsonObject.toString());

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                    }
                });

        HttpBaseUtil.getHttpBaseUtil().post2("hospital/getHospitalList", map,
                this, new HttpBaseUtil.HttpListener() {
                    @Override
                    public void onSecussful(String msg) {
                        try {
                            JSONObject response = new JSONObject(msg);
                            Log.d("MainActivity response2", response.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFail(int i, Exception e, String s) {
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(s);
                            Log.d("MainActivity fail2", jsonObject.toString());

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                    }
                });

        HttpBaseUtil.getHttpBaseUtil().post2("doctor/getDoctorList", map,
                this, new HttpBaseUtil.HttpListener() {
                    @Override
                    public void onSecussful(String msg) {
                        try {
                            JSONObject response = new JSONObject(msg);
                            Log.d("MainActivity response3", response.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFail(int i, Exception e, String s) {
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(s);
                            Log.d("MainActivity fail3", jsonObject.toString());

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                    }
                });
    }
}