package com.jimbo.hospital;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Warren on 2018/3/7.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.Holder> {

    private String[] images;
    private Context context;

    public RecyclerViewAdapter(Context context, String[] images) {
        this.images = images;
        this.context = context;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.doctor_recycler_item, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        // final int image = images[position];
        //  Random rnd = new Random();
        // int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        //holder.frameLayout.setBackgroundColor(color);
        //   holder.title.setText(mockObject.getTitle());
        //holder.imageView.setImageResource(images[position]);
        //  Glide.with(context).load(images[position]).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).into(holder.imageView);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DoctorProfileActivity.class);
                intent.putExtra("state", 2);
                context.startActivity(intent);

                //Toast.makeText(v.getContext(), "clicked!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (images != null) {
            return images.length;
        }
        return 0;
    }

    static class Holder extends RecyclerView.ViewHolder {
        FrameLayout frameLayout;
        ImageView imageView;

        public Holder(View itemView) {
            super(itemView);
            //frameLayout = itemView.findViewById(R.id.fl_container);
            imageView = itemView.findViewById(R.id.iv_image);
        }
    }
}
