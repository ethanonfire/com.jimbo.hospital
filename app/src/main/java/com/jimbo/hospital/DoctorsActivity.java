package com.jimbo.hospital;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.jimbo.hospital.fragments.DetailFragment;

public class DoctorsActivity extends AppCompatActivity {

    int state = 0;
    ImageButton imageButton;
    //RecyclerView recyclerView;
    Button domastic;
    Button oversea;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctors);
        state = getIntent().getIntExtra("state", 1);
        domastic = findViewById(R.id.domastic);
        domastic.setSelected(true);
        domastic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DetailFragment secondFragment = new DetailFragment();

                Bundle args = new Bundle();
                args.putInt("position", 0);
                secondFragment.setArguments(args);          // (1) Communicate with Fragment using Bundle

                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.flContainer2, secondFragment) // replace flContainer
                            //.addToBackStack(null)
                            .commit();
                }
                domastic.setSelected(true);
                oversea.setSelected(false);
                domastic.setTextColor(getResources().getColor(R.color.white));
            }
        });
        oversea = findViewById(R.id.oversea);

        oversea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DetailFragment secondFragment = new DetailFragment();

                Bundle args = new Bundle();
                args.putInt("position", 1);
                secondFragment.setArguments(args);          // (1) Communicate with Fragment using Bundle

                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.flContainer2, secondFragment) // replace flContainer
                            //.addToBackStack(null)
                            .commit();
                }
                domastic.setSelected(false);
                oversea.setSelected(true);
                domastic.setTextColor(getResources().getColor(R.color.black));
            }
        });
        Log.d("DEBUG", getResources().getConfiguration().orientation + "");

//        if (savedInstanceState == null) {
//            // Instance of first fragment
//            MenuFragment firstFragment = new MenuFragment();
//
//            // Add Fragment to FrameLayout (flContainer), using FragmentManager
//            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();// begin  FragmentTransaction
//            ft.add(R.id.flContainer, firstFragment);                                // add    Fragment
//            ft.commit();                                                            // commit FragmentTransaction
//        }

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Log.d("DoctorsActivity", "reached here");
            DetailFragment secondFragment = new DetailFragment();
            Bundle args = new Bundle();
            args.putInt("position", 0);
            secondFragment.setArguments(args);          // (1) Communicate with Fragment using Bundle
            FragmentTransaction ft2 = getSupportFragmentManager().beginTransaction();// begin  FragmentTransaction
            ft2.add(R.id.flContainer2, secondFragment);                               // add    Fragment
            ft2.commit();                                                            // commit FragmentTransaction
        }

        // recyclerView = findViewById(R.id.recycler_view);
        // setupRecyclerView();
        imageButton = findViewById(R.id.iv_back_activity);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

//    @Override
//    public void onPizzaItemSelected(int position) {
//        //  Toast.makeText(this, "Called By Fragment A: position - "+ position, Toast.LENGTH_SHORT).show();
//
//        // Load Data_Doctor Detail Fragment
//        DetailFragment secondFragment = new DetailFragment();
//
//        Bundle args = new Bundle();
//        args.putInt("position", position);
//        secondFragment.setArguments(args);          // (1) Communicate with Fragment using Bundle
//
//        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            getSupportFragmentManager()
//                    .beginTransaction()
//                    .replace(R.id.flContainer2, secondFragment) // replace flContainer
//                    //.addToBackStack(null)
//                    .commit();
//        }
////        else {
////            getSupportFragmentManager()
////                    .beginTransaction()
////                    .replace(R.id.flContainer, secondFragment) // replace flContainer
////                    .addToBackStack(null)
////                    .commit();
////        }
//    }
}
