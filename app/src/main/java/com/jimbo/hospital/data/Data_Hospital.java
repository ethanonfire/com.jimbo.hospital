package com.jimbo.hospital.data;

/**
 * Created by Shyam Rokde on 8/5/16.
 */
public class Data_Hospital {

    String name;
    String introduction;
    String imageUrl;

    public Data_Hospital(String name, String introduction, String imageUrl) {
        this.name = name;
        this.introduction = introduction;
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
