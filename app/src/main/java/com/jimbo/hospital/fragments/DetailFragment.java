package com.jimbo.hospital.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jimbo.hospital.GridVerticalSpacingItemDecoration;
import com.jimbo.hospital.R;
import com.jimbo.hospital.RecyclerViewAdapter;

/**
 * Created by Shyam Rokde on 8/5/16.
 */
public class DetailFragment extends Fragment {
    int position = 0;
    //  TextView tvTitle;
//  TextView tvDetails;
    //ImageView iv_doctor_info;
    int[] data = new int[4];
    // SubsamplingScaleImageView iv_doctor_info;
    int state = 1;
    RecyclerView recyclerView;

    public DetailFragment() {
        //this.state = state;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            // Get back arguments
            if (getArguments() != null) {
                position = getArguments().getInt("position", 0);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {

        // Inflate the xml file for the fragment
        View view = inflater.inflate(R.layout.layout_doctors_recycler, parent, false);
        //  recyclerView = view.findViewById(R.id.recycler_view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        recyclerView = view.findViewById(R.id.recycler_view);
        setupRecyclerView();
//    // Set values for view here
        // iv_doctor_info = (SubsamplingScaleImageView) view.findViewById(R.id.iv_doctor_info);

//        switch (state) {
//            case 1:
//                data = Data_Hospital.dataDetail;
//                break;
//            case 2:
//                data = Data_Speciality.dataDetail;
//                break;
//            case 3:
//                data = Data_Doctor.dataDetail;
//                break;
//        }

        //iv_doctor_info.setImage(ImageSource.resource(data[position]));
    }

    // Activity is calling this to update view on Fragment
    public void updateView(int position) {

        //iv_doctor_info.setImage(ImageSource.resource(data[position]));
//    Glide.with(getContext())
//            .load(Data_Doctor.pizzaDetails[position])
//            .into(iv_doctor_info);
//    tvTitle.setText(Data_Doctor.pizzaMenu[position]);
//    tvDetails.setText(Data_Doctor.pizzaDetails[position]);
    }

    private void setupRecyclerView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);

        recyclerView.setLayoutManager(gridLayoutManager);
        String[] image0 = {
                "https://upload.wikimedia.org/wikipedia/commons/8/8e/G%C3%A9rald_KIERZEK_%28Cr%C3%A9dit_Ibo%29.jpg",
                "https://upload.wikimedia.org/wikipedia/commons/8/8e/G%C3%A9rald_KIERZEK_%28Cr%C3%A9dit_Ibo%29.jpg",
                "https://upload.wikimedia.org/wikipedia/commons/8/8e/G%C3%A9rald_KIERZEK_%28Cr%C3%A9dit_Ibo%29.jpg",
                "https://upload.wikimedia.org/wikipedia/commons/8/8e/G%C3%A9rald_KIERZEK_%28Cr%C3%A9dit_Ibo%29.jpg",
                "https://upload.wikimedia.org/wikipedia/commons/8/8e/G%C3%A9rald_KIERZEK_%28Cr%C3%A9dit_Ibo%29.jpg",
                "https://upload.wikimedia.org/wikipedia/commons/8/8e/G%C3%A9rald_KIERZEK_%28Cr%C3%A9dit_Ibo%29.jpg",
                "https://upload.wikimedia.org/wikipedia/commons/8/8e/G%C3%A9rald_KIERZEK_%28Cr%C3%A9dit_Ibo%29.jpg",

        };

        String[] image1 = {

                "https://www.pinnaclecare.com/wp-content/uploads/2017/12/bigstock-African-young-doctor-portrait-28825394.jpg.webp",
                "https://www.pinnaclecare.com/wp-content/uploads/2017/12/bigstock-African-young-doctor-portrait-28825394.jpg.webp",
                "https://www.pinnaclecare.com/wp-content/uploads/2017/12/bigstock-African-young-doctor-portrait-28825394.jpg.webp",
                "https://www.pinnaclecare.com/wp-content/uploads/2017/12/bigstock-African-young-doctor-portrait-28825394.jpg.webp",
                "https://www.pinnaclecare.com/wp-content/uploads/2017/12/bigstock-African-young-doctor-portrait-28825394.jpg.webp",
                "https://www.pinnaclecare.com/wp-content/uploads/2017/12/bigstock-African-young-doctor-portrait-28825394.jpg.webp",
        };

        RecyclerViewAdapter adapter = null;
        if (position == 0) {
            adapter = new RecyclerViewAdapter(getContext(), image0);

        } else {
            adapter = new RecyclerViewAdapter(getContext(), image1);

        }
        // RecyclerViewAdapter adapter = new RecyclerViewAdapter(image);
        recyclerView.setAdapter(adapter);
//
        GridVerticalSpacingItemDecoration gridVerticalSpacingItemDecoration = new GridVerticalSpacingItemDecoration(50, 3);
        recyclerView.addItemDecoration(gridVerticalSpacingItemDecoration);
    }
}
