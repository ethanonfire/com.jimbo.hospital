package com.jimbo.hospital;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import androidx.annotation.NonNull;

import com.jimbo.jimbohttp.HttpBaseUtil;
import com.jimbo.project.BackAidlListener;
import com.jimbo.project.JimboService;

import static android.content.Context.BIND_AUTO_CREATE;

public class AIDLUtil {
    public static String TAG = "AIDLUtil";
    public static String token;
    public static String objid;
    public static String sn;
    public static String key;
    public static boolean isSleep = true;
    private static AIDLUtil aidlUtil;
    private JimboService iRemoteService;
    private BackAidlListener iRemoteCallback;
    private Context BaseContext;
    private boolean isBindSuccess = false;
    private BindServiceListener mBindServiceLinster;
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            iRemoteService = JimboService.Stub.asInterface(service);
            try {
                Log.e(TAG, "bindService   success");
                sendActiviteApplication(true);
                isBindSuccess = true;
                iRemoteService.register(iRemoteCallback);
                token = iRemoteService.token();
                objid = iRemoteService.objId();
                sn = iRemoteService.Sn();
                key = iRemoteService.Sn_Ket();
                Log.d(TAG, "token: " + token + " objid: " + objid + " sn: " + sn + " key: " + key);
                HttpBaseUtil.getHttpBaseUtil().setAllData(token, objid, sn, key);

                if (mBindServiceLinster != null) {
                    mBindServiceLinster.onSuccess();
                }
                //
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            iRemoteService = null;
            iRemoteCallback = null;
        }
    };

    private AIDLUtil() {
    }

    public static AIDLUtil getInstance() {
        if (null == aidlUtil) {
            synchronized (AIDLUtil.class) {
                if (null == aidlUtil) {
                    aidlUtil = new AIDLUtil();
                }
            }
        }
        return aidlUtil;
    }

    public void bindService(Context mContext, @NonNull BackAidlListener iRemoteCallback) {
        this.iRemoteCallback = iRemoteCallback;
        if (iRemoteService != null) {
            return;
        }
        BaseContext = mContext;
        Intent bindService = new Intent();
        bindService.setAction("com.jimbo.project.main.aidl");
        bindService.setPackage("com.jimbo.project");
        mContext.bindService(bindService, connection, BIND_AUTO_CREATE);
        MyApplication.getMyApplication().setAidlListener(new MyApplication.AidlListener() {
            @Override
            public void updateToken() {
                try {
                    token = iRemoteService.token();
                    objid = iRemoteService.objId();
                    HttpBaseUtil.getHttpBaseUtil().setTokenObjid(token, objid);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void unbindService(Context mContext) {

        if (null == iRemoteService) {
            Log.e("aidl_show", "unbindService  iRemoteService error");
            return;
        }
        if (BaseContext != mContext) {
            Log.e("aidl_show", "unbindService  Context error");
            return;
        }
        Log.e("aidl_show", "unbindService  success");
        try {
            if (null != iRemoteCallback) {
                iRemoteService.unRegister(iRemoteCallback);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        mContext.unbindService(connection);
        iRemoteService = null;
        iRemoteCallback = null;
    }

    public void send(String msg) {
        Log.e(TAG, msg + "      " + (null == iRemoteService));
        if (null == iRemoteService) {
            return;
        }
        try {
            iRemoteService.mainTask(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void sendNoSleep(boolean state) {//是否不待机
        String json = "{\"status\":3,\"data\":" + state + "}";
        AIDLUtil.getInstance().send(json);
    }

    public void AidlStatus(int status) {
        if (isSleep) {
            if (status == 0) {
                //    ActivityUtil.getActivityUtil().closeAll();
            }
        }
    }

    public void sendActiviteApplication(boolean state) {
        if (iRemoteService == null) {
            return;
        }
        try {
            String json = "{\"isAddProcess\":" + state + ",\"packageName\":\"" + MyApplication.getApplication().getPackageName() + "\"}";
            iRemoteService.setPackage(json);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void setmBindServiceLinster(BindServiceListener mBindServiceLinster) {
        this.mBindServiceLinster = mBindServiceLinster;
    }

    public JimboService getiRemoteService() {
        return iRemoteService;
    }

    public interface BindServiceListener {
        void onSuccess();
    }
}

