package com.jimbo.hospital;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.jimbo.project.BackAidlListener;

import java.util.ArrayList;

public class MyApplication extends Application {

    public static boolean AIDL_Bind = false;
    public static String TAG = "MyApplication";
    private static MyApplication myApplication;
    public AidlServiceListener aidlServiceListener;
    private ArrayList<Activity> activityList = new ArrayList();
    private AidlListener aidlListener;

    // private static final String APP_ID = "8f7cafe6ac";
    public static MyApplication getApplication() {
        return myApplication;
    }

    public static MyApplication getMyApplication() {
        return myApplication;
    }

    public void setAidlServiceListener(AidlServiceListener aidlServiceListener) {
        this.aidlServiceListener = aidlServiceListener;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");

        myApplication = this;
        // CrashReport.initCrashReport(getApplicationContext(), APP_ID, false);
        AIDLUtil.getInstance().bindService(MyApplication.this, new BackAidlListener.Stub() {
            @Override
            public void onBack(final String message, int status) throws RemoteException {
                switch (status) {
                    case 0:
                        for (int i = 0; i < activityList.size(); i++) {
                            activityList.get(i).finish();
                        }
                        android.os.Process.killProcess(android.os.Process.myPid());
                        break;
                    case 13:
                        if (aidlListener != null) {
                            aidlListener.updateToken();
                        }
                        break;
                }
            }
        });
        AIDLUtil.getInstance().setmBindServiceLinster(new AIDLUtil.BindServiceListener() {
            @Override
            public void onSuccess() {
                AIDL_Bind = true;
                Log.d(TAG, "bind successful");
                if (aidlServiceListener != null) {
                    aidlServiceListener.onBindSuccess();
                }
            }
        });

        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                activityList.add(activity);
                Log.d(TAG, "activity created： " + activityList.size());

            }

            @Override
            public void onActivityStarted(Activity activity) {

            }

            @Override
            public void onActivityResumed(Activity activity) {

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                activityList.remove(activity);
                Log.d(TAG, "activity removed： " + activityList.size());
                int sum = activityList.size();
                if (sum == 0) {
                    AIDLUtil.getInstance().sendActiviteApplication(false);
                    AIDLUtil.getInstance().unbindService(MyApplication.this);
                    android.os.Process.killProcess(android.os.Process.myPid());
                }
            }
        });
    }

    public void setAidlListener(AidlListener aidlListener) {
        this.aidlListener = aidlListener;
    }

    public interface AidlListener {
        void updateToken();
    }

//    public void setOnWayPointCorrReceivedCallback(OnLauncherMessageReceivedCallback callback) {
//        this.onLauncherMessageReceivedCallback = callback;
//    }
//
//    public interface OnLauncherMessageReceivedCallback {
//        void onWayPointReceived(MapWayPointBean wayPointBean);
//
//        void onMapReceived(String map);
//
//    }

    public interface AidlServiceListener {

        void onBindSuccess();
    }
}
