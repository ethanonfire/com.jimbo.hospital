package com.jimbo.hospital;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jimbo.hospital.data.Data_Hospital;
import com.jimbo.jimbohttp.HttpBaseUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HospitalIntroActivity extends AppCompatActivity {
//

    TextView tv_introduction;
    ImageView iv_image;
    ArrayList<Data_Hospital> Hospitals = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_intro);

        ImageButton ib_back = findViewById(R.id.iv_back_app);
        tv_introduction = findViewById(R.id.tv_introduction);
        iv_image = findViewById(R.id.iv_image);

        final TextView bt_button1 = findViewById(R.id.tv_button1);
        final TextView bt_button2 = findViewById(R.id.tv_button2);
        bt_button1.setTextColor(getColor(R.color.logoBlue));

        bt_button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bt_button1.setTextColor(getColor(R.color.logoBlue));
                bt_button2.setTextColor(getColor(R.color.white));
            }
        });
        bt_button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bt_button1.setTextColor(getColor(R.color.white));
                bt_button2.setTextColor(getColor(R.color.logoBlue));
            }
        });
        ib_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Map<String, String> map = new HashMap<>();
        map.put("robotId", AIDLUtil.objid);
        HttpBaseUtil.getHttpBaseUtil().post2("hospital/getHospitalList", map,
                this, new HttpBaseUtil.HttpListener() {
                    @Override
                    public void onSecussful(String msg) {
                        try {
                            JSONObject response = new JSONObject(msg);
                            JSONArray list = response.getJSONObject("result").getJSONArray("list");
                            for (int i = 0; i < list.length(); i++) {
                                String name = list.getJSONObject(i).getString("name");
                                String introduce = list.getJSONObject(i).getString("introduce");
                                //String imageUrl = list.getJSONObject(i).getString("imageUrl");
                                Hospitals.add(new Data_Hospital(name, introduce, "https://helpx.adobe.com/content/dam/help/en/photoshop/using/convert-color-image-black-white/jcr_content/main-pars/before_and_after/image-before/Landscape-Color.jpg"));
                            }

                            tv_introduction.setText(Hospitals.get(0).getIntroduction());
                            Glide.with(getApplicationContext()).load(Hospitals.get(0).getImageUrl()).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).into(iv_image);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFail(int i, Exception e, String s) {
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(s);
                            Log.d("MainActivity fail2", jsonObject.toString());

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                    }
                });

    }

}
